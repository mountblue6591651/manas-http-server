const http = require("http");
const fs = require("fs").promises;
const uuid4 = require("uuid4");

async function handler() {
  try {
    // Read the contents of 'index.html' and 'data.json' asynchronously
    const ans = await fs.readFile("index.html", "utf-8");
    const jsonData = await fs.readFile("data.json", "utf-8");

    // Create an HTTP server
    const server = http.createServer((req, res) => {
      try {
        // Handle different routes based on the requested URL
        if (req.url === "/html") {
          // Respond with 'index.html' content
          res.writeHead(200, { "Content-Type": "text/html" });
          res.write(ans);
          res.end();
        } else if (req.url === "/json") {
          // Respond with 'data.json' content
          res.writeHead(200, { "Content-Type": "application/json" });
          res.write(jsonData);
          res.end();
        } else if (req.url === "/uuid") {
          // Respond with a generated UUID
          res.writeHead(200, { "Content-Type": "application/json" });
          res.write(uuid4());
          res.end();
        } else if (req.url.includes("/status")) {
          // Respond with a custom status code and a message
          const statusCode = req.url.split("/")[2];
          res.writeHead(Number(statusCode), { "Content-Type": "text/html" });
          res.write(
            `<h2>Return a response with ${statusCode} status code</h2>`
          );
          res.end();
        } else if (req.url.includes("/delay")) {
          // Respond after a delay with a message
          const delay = req.url.split("/")[2];

          setTimeout(() => {
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.write(`Response after ${delay} seconds.`);
            res.end();
          }, Number(delay) * 1000);
        }
      } catch (error) {
        // Handle errors and respond with a 404 Not Found status
        res.writeHead(404, { "Content-Type": "text/html" });
        res.write("<h1>Webpage not found :)</h1>");
        res.end();
      }
    });

    // Start the server and listen on port 3000
    server.listen(3000, () => {
      console.log("Server running at http://localhost:3000");
    });
  } catch (error) {
    // Handle errors during file reading and log the error message
    console.log(error.message);
  }
}

// Call the handler function to start the server
handler();
